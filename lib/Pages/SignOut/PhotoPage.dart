import 'package:flutter/material.dart';

class PhotoPage extends StatelessWidget {
  const PhotoPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Stack(
          children: [
            Container(
              height: 850,
              child: Image.asset(
                'images/home.jpeg',
                fit: BoxFit.fill,
              ),
            ),
            Positioned(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.pushNamed(context, 'loginPage');
                    },
                    child: Container(
                      margin: EdgeInsets.only(top: 750, left: 220),
                      width: 150,
                      height: 50,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        color: Color.fromARGB(251, 237, 203, 234),
                      ),
                      child: Center(
                        child: Text(
                          "Нэвтрэх",
                          style: TextStyle(
                              color: Color.fromARGB(255, 234, 160, 129),
                              fontSize: 18,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
