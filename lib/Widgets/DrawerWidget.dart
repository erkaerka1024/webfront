import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DrawerWidget extends StatelessWidget {
  const DrawerWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          DrawerHeader(
            padding: EdgeInsets.zero,
            child: UserAccountsDrawerHeader(
              decoration:
                  BoxDecoration(color: Color.fromARGB(255, 192, 170, 255)),
              accountName: Text(
                "Хөгжүүлэгч",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              accountEmail: Text(
                "programmer@gmail.com",
                style: TextStyle(
                  fontSize: 16,
                ),
              ),
              currentAccountPicture: CircleAvatar(
                backgroundImage: AssetImage("images/avatar.jpg"),
              ),
            ),
          ),
          InkWell(
            onTap: () {
              Navigator.pushNamed(context, "homePage");
            },
            child: ListTile(
              leading: Icon(
                CupertinoIcons.home,
                color: Color.fromRGBO(240, 212, 246, 1),
              ),
              title: Text(
                "Нүүр",
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
          InkWell(
            onTap: () {
              Navigator.pushNamed(context, "Profile");
            },
            child: ListTile(
              leading: Icon(
                CupertinoIcons.person,
                color: Color.fromRGBO(240, 212, 246, 1),
              ),
              title: Text(
                "Миний бүртгэл",
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
          ListTile(
            leading: Icon(
              CupertinoIcons.cart_fill,
              color: Color.fromRGBO(240, 212, 246, 1),
            ),
            title: Text(
              "Миний худалдан авалт",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          InkWell(
            onTap: () {
              Navigator.pushNamed(context, "favoritePage");
            },
            child: ListTile(
              leading: Icon(
                CupertinoIcons.heart_fill,
                color: Color.fromRGBO(240, 212, 246, 1),
              ),
              title: Text(
                "Таалагдсан",
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
          InkWell(
            onTap: () {
              Navigator.pushNamed(context, 'loginPage');
            },
            child: ListTile(
              leading: Icon(
                Icons.exit_to_app,
                color: Color.fromRGBO(240, 212, 246, 1),
              ),
              title: Text(
                "Гарах",
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
