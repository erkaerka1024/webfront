import 'package:erka/Pages/SignIn/CartPage.dart';
import 'package:erka/Pages/SignIn/FavoritePage.dart';
import 'package:erka/Pages/SignIn/ItemPage.dart';
import 'package:erka/Pages/SignIn/ProfilePage.dart';
import 'package:erka/Pages/SignOut/LoginPage.dart';
import 'package:erka/Pages/SignOut/PhotoPage.dart';
import 'package:erka/Pages/SignOut/Register1Page.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'Pages/SignIn/HomePage.dart';
import 'firebase_options.dart';

void main() async {
  runApp(const Food());
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
}

class Food extends StatelessWidget {
  const Food({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "Food App",
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          scaffoldBackgroundColor: Color(0xFFF5F5F3),
        ),
        routes: {
          "/": (context) => PhotoPage(),
          "loginPage": (context) => LoginPage(),
          "homePage": (context) => HomePage(),
          "cartPage": (context) => CartPage(),
          "itemPage": (context) => ItemPage(),
          "Profile": (context) => ProfilePage(),
          "register1": (context) => Register1Page(),
          "favoritePage": (context) => FavoritePage(),
        });
  }
}
